<?php
	global $DB,$OUTPUT,$PAGE,$USER, $CFG,$SITE;
	require_once("../../config.php");
	$PAGE->set_pagelayout('login');
	$PAGE->set_url('/local/apropos/index.php');
	$context = context_system::instance();
	$PAGE->set_context($context);
	$PAGE->set_title($SITE->shortname);
	$PAGE->set_heading($SITE->fullname);
 ?>

	<?php echo $OUTPUT->header(); ?>

<article style='width:80%;margin:0 auto;'>
<h1 style='text-align:center;'>Mentions légales du service ÉLÉA</h1>


<h2>Présentation</h2>

<p>La plateforme ÉLÉA est un service de l'académie de Versailles. Elle propose un service de e-education réservé aux enseignants et personnels administratifs de l'académie de Versailles.</p>

<h2>Hébergeur</h2>

<address>
<p>La Dane de l'académie de Versailles</p>
<p>2 rue Pierre Bourdan - 78160 MARLY-LE-ROI</p>
</address>
<ul>
<li>Maintenance technique : service web de la Dane de l'académie de Versailles</li>
<li>Contact : <a href='mailto:support-elea@ac-versailles.fr'>support-elea@ac-versailles.fr</a></li>
<li>Rejoignez la <a href='http://www.viaeduc.fr/group/10245'>communauté dédiée sur ViaÉduc</a></li>

</ul>

<h2>Droit d'auteur et de reproduction</h2>

<p>Les informations utilisées ne doivent l'être qu'à des fins personnelles, associatives ou professionnelles ; toute utilisation ou reproduction à des fins commerciales ou publicitaires est interdite.</p>
<p>La reproduction, par n'importe quel moyen et à n'importe quelle fin, des documents non publics, des graphismes, photographies et ressources multimédias issus des vidéos hébergées par la Dane de l'académie de Versailles est soumise à l'accord préalable de leurs auteurs ou ayants droit en tenant compte des licences associées.</p>

<p>Toute utilisation partielle ou totale de ce service est soumise au respect des licences associées.</p>


<h2>Données personnelles</h2>

<p>Aucune information personnelle n'est cédée à des tiers.</p>

<h2>Données statistiques</h2>

<p>Afin de mieux connaître les centres d'intérêt des visiteurs du site et en vue de son amélioration, nous pouvons être amenés à mesurer le nombre de visites, de pages vues, ainsi que l'activité des visiteurs sur le site, et leur fréquence de retour. A cet effet, la technologie des cookies peut être utilisée. Il vous est possible de refuser ces "cookies" ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux vidéos du site. Pour vous opposer à l'enregistrement de cookies ou être prévenu avant d'accepter les cookies, nous vous recommandons la lecture de la rubrique d'aide de votre navigateur qui vous précisera la marche à suivre.</p>

<p>Par ailleurs, la Dane de l'académie de Versailles procède à l'analyse de la fréquentation du site à partir de l'exploitation des données de connexion. Les adresses IP relatives aux consultations effectuées ne sont pas conservées au-delà de 12 mois, afin de respecter les obligations légales.</p>


<h2>Conditions d'utilisation du site</h2>

<p>L'utilisateur de ce site reconnaît disposer de la compétence et des moyens nécessaires pour accéder et utiliser ce site. Il est invité à respecter les bons usages de l'internet, en particulier, dans l'utilisation des adresses électroniques qui y figurent. Il est rappelé que l'article 26 de la loi du 6 janvier 1978 « Informatique et libertés » prohibe toute collecte massive de ces adresses, à l'insu de leurs détenteurs, pour procéder à l'envoi massif de messages non désirés, quel que soit l'objet des messages diffusés. En application de l'article 16 de cette loi, sont également interdites la constitution de systèmes d'envoi automatisé de messages, la création de bases de données réunissant les adresses électroniques et la mise en place de traitements automatisés d'informations nominatives le concernant qui n'auraient pas fait l'objet d'une déclaration préalable à la CNIL.</p>
</article>
	<?php echo $OUTPUT->footer(); ?>
